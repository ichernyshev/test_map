let builtinIteratee;

const _iteratee = builtinIteratee = function(value, context) {
    return cb(value, context, Infinity);
};

const _identity = function(value) {
    return value;
};

const _isMatch = function(object, attrs) {
    var keys = _keys(attrs), length = keys.length;
    if (object == null) return !length;
    var obj = Object(object);
    for (var i = 0; i < length; i++) {
      var key = keys[i];
      if (attrs[key] !== obj[key] || !(key in obj)) return false;
    }
    return true;
};

const _matches = function(attrs) {
    attrs = Object.assign({}, attrs);
    return function(obj) {
      return _isMatch(obj, attrs);
    };
};

const cb = function(value, context, argCount) {
    if (_iteratee !== builtinIteratee) return _iteratee(value, context);
    if (value == null) return _identity;
    if (typeof(value) === 'function') return optimizeCb(value, context, argCount);
    if (_isObject(value)) return _matches(value);
    return property(value);
};

const _isObject = function(obj) {
    var type = typeof obj;
    return type === 'function' || type === 'object' && !!obj;
};

const _keys = function(obj) {
    if (!_isObject(obj)) return [];
    if (Object.keys) return Object.keys(obj);
};

const _values = function(obj) {
    let keys = _keys(obj);
    let length = keys.length;
    let values = Array(length);
    for (let i = 0; i < length; i++) {
        values[i] = obj[keys[i]];
    }
    return values;
};
  
const property = function(key) {
    return function(obj) {
        return obj == null ? void 0 : obj[key];
    };
};
  
let MAX_ARRAY_INDEX = Math.pow(2, 53) - 1;
let getLength = property('length');
  
const isArrayLike = function(collection) {
    let length = getLength(collection);
    return typeof length == 'number' && length >= 0 && length <= MAX_ARRAY_INDEX;
};

var optimizeCb = function(func, context, argCount) {
    if (context === void 0) return func;
    switch (argCount == null ? 3 : argCount) {
      case 1: return function(value) {
        return func.call(context, value);
      };
      // The 2-parameter case has been omitted only because no current consumers
      // made use of it.
      case 3: return function(value, index, collection) {
        return func.call(context, value, index, collection);
      };
      case 4: return function(accumulator, value, index, collection) {
        return func.call(context, accumulator, value, index, collection);
      };
    }
    return function() {
      return func.apply(context, arguments);
    };
};

const forEach = function(obj, iteratee, context) {
    iteratee = optimizeCb(iteratee, context);
    let i, length;
    if (isArrayLike(obj)) {
        for (i = 0, length = obj.length; i < length; i++) {
        iteratee(obj[i], i, obj);
        }
    } else {
        let keys = _keys(obj);
        for (i = 0, length = keys.length; i < length; i++) {
        iteratee(obj[keys[i]], keys[i], obj);
        }
    }
    return obj;
};
  
const min = function(obj, iteratee, context) {
    let result = Infinity, lastComputed = Infinity,
        value, computed;
    if (iteratee == null || (typeof iteratee == 'number' && typeof obj[0] != 'object') && obj != null) {
        obj = isArrayLike(obj) ? obj : _values(obj);
        for (let i = 0, length = obj.length; i < length; i++) {
        value = obj[i];
        if (value != null && value < result) {
            result = value;
        }
        }
    } else {
        iteratee = cb(iteratee, context);
        forEach(obj, function(v, index, list) {
        computed = iteratee(v, index, list);
        if (computed < lastComputed || computed === Infinity && result === Infinity) {
            result = v;
            lastComputed = computed;
        }
        });
    }
    return result;
};
  
export default min;